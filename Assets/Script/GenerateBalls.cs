using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenerateBalls : MonoBehaviour
{
    public GameObject[] ballsCreated;
    public Text ballsAmount;
    public float ballsMass;
    public float ballsRadius;
    public int ballsProximityModifier;
    public int howManyBallsToGenerate;
    public int ballsHowManyBallsEaten;
    bool isBallsGenerating = true;
    void Start()
    {
        StartCoroutine(GenerateCoroutine());
        
    }

    IEnumerator GenerateCoroutine()
    {
        while(isBallsGenerating)
        {
            yield return new WaitForSeconds(0.25f);
            GameObject balls = ObjectPooler.SharedInstance.GetPooledObject(); 
            if (balls != null) 
            {
                balls.transform.position = new Vector2(Random.Range(-5.68f, 5.68f), Random.Range(-4.88f, 4.88f));
                balls.transform.rotation = gameObject.transform.rotation;
                balls.GetComponent<CircleScript>().mass = ballsMass;
                balls.GetComponent<CircleScript>().radius = ballsRadius;
                balls.GetComponent<CircleScript>().proximityModifier = ballsProximityModifier;
                balls.GetComponent<CircleScript>().howManyBallsEaten = ballsHowManyBallsEaten;
                balls.SetActive(true);
                ballsCreated = GameObject.FindGameObjectsWithTag("Balls");
                ballsAmount.text = "" + ballsCreated.Length;
                if(ballsCreated.Length>=howManyBallsToGenerate)
                {
                    isBallsGenerating = false;
                    foreach (GameObject ballsGenerated in ballsCreated) 
                    {
                        ballsGenerated.GetComponent<CircleScript>().toManyBalls = true;
                    }
                }
            }
        }
    }
}
