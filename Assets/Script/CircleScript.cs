using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleScript : MonoBehaviour
{

	public float mass;    
	public float radius;
	public int proximityModifier;	
    public bool toManyBalls;
    public int howManyBallsEaten;
	
	public void OnDrawGizmos() 
    {
		Gizmos.DrawWireSphere (transform.position, radius);
	}
	
	void FixedUpdate () 
    { 
	GameObject[] ballsAffectedByGravity;
    ballsAffectedByGravity = GameObject.FindGameObjectsWithTag ("Balls");
		foreach (GameObject gravBody in ballsAffectedByGravity) { 
			Rigidbody2D ballRigidBody = gravBody.GetComponent<Rigidbody2D> (); 
			float gravityDistance = Vector3.Distance (transform.position, ballRigidBody.transform.position); 
			if (gravityDistance < radius) 
            {
				Vector3 objectOffset = transform.position - ballRigidBody.transform.position; 
				objectOffset.z = 0;
				Vector3 objectTrajectory = ballRigidBody.velocity; 
				float angle = Vector3.Angle (objectOffset, objectTrajectory); 
				float magsqr = objectOffset.sqrMagnitude; 
				if ( magsqr > 0.0001f ) { 
					Vector3 gravityVector = ( mass * objectOffset.normalized / magsqr ) * ballRigidBody.mass;
                    if(toManyBalls==true)
                    {
                        ballRigidBody.AddForce ( -gravityVector * ( gravityDistance/proximityModifier) );
                    }
                    else
                    {
                        ballRigidBody.AddForce ( gravityVector * ( gravityDistance/proximityModifier) );
                    }
				}
			} 
		}
	}
    private void OnCollisionEnter2D(Collision2D other) 
    {
        if(toManyBalls==false)
        {
            gameObject.SetActive(false);
            other.gameObject.SetActive(false);
            GameObject balls = ObjectPooler.SharedInstance.GetPooledObject(); 
            balls.transform.position = transform.position;
            balls.transform.rotation = gameObject.transform.rotation;
            balls.GetComponent<CircleScript>().mass += other.gameObject.GetComponent<CircleScript>().mass;
            balls.GetComponent<CircleScript>().radius += other.gameObject.GetComponent<CircleScript>().radius;
            balls.GetComponent<CircleScript>().proximityModifier += other.gameObject.GetComponent<CircleScript>().proximityModifier;
            
            balls.transform.localScale = new Vector3(balls.transform.localScale.x + other.gameObject.transform.localScale.x, other.gameObject.transform.localScale.y + gameObject.transform.localScale.y, other.gameObject.transform.localScale.z + gameObject.transform.localScale.z);

            balls.SetActive(true);
            howManyBallsEaten++;
            if(mass>=4 && other.gameObject.GetComponent<CircleScript>().mass >= 4)
            {
                gameObject.SetActive(false);
                for(int i = 0 ; i<=howManyBallsEaten; i++)
                {
                    CreateNewBall();
                }
            }
        }
    }
    private void CreateNewBall()
    {
        GameObject balls = ObjectPooler.SharedInstance.GetPooledObject(); 
        balls.transform.position = transform.position;
        balls.transform.rotation = gameObject.transform.rotation;
        balls.GetComponent<CircleScript>().mass = 1;
        balls.GetComponent<CircleScript>().radius = 1;
        balls.GetComponent<CircleScript>().proximityModifier = 1 ;
        balls.GetComponent<CircleScript>().howManyBallsEaten = 1 ;
        balls.SetActive(true);
    }
}

